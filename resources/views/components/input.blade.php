@props(['name'])

<input name="{{ $name }}"
    {!! $attributes->merge(['class' => 'block w-full px-4 py-2 mt-2 text-gray-700 bg-white border border-gray-300 rounded-md focus:border-blue-500 focus:outline-none']) !!}
/>
@error($name)
    <span class="text-red-500 text-sm font-semibold"> {{ $message }}</span>
@enderror
