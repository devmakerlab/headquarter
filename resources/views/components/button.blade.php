<button {{ $attributes->merge(['type' => 'submit', 'class' => 'bg-blue-600 hover:bg-blue-700 text-white font-bold py-2 rounded shadow w-full duration-200 mt-8']) }}>
    {{ $slot }}
</button>
