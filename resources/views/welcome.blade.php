<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>{{ config('app.name', 'Laravel') }}</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Roboto&display=swap" rel="stylesheet">

        <!-- Styles -->
        <link rel="stylesheet" href="{{ asset('css/app.css') }}">

        <!-- Scripts -->
        <script src="{{ asset('js/app.js') }}" defer></script>
    </head>
    <body style="font-family: 'Roboto', sans-serif;" class="antialiased bg-watermark">
        <header class="w-full flex p-5 border-white border-b-2 text-white">
            <a class="flex-1 font-extrabold text-xl" href="/">
                {{ config('app.name', 'Laravel') }}
            </a>
            <div>
                <ul class="list-reset flex justify-end flex-1 items-center">
                    @guest
                        <li class="mr-3">
                            <a class="no-underline hover:text-gray-500 py-2 px-4" href="{{ route('login') }}">
                                {{ __('Sign in') }}
                            </a>
                        </li>
                        <li class="mr-3">
                            <a class="no-underline border rounded border-white hover:text-gray-500 hover:border-gray-500 py-2 px-4" href="{{ route('register') }}">
                                {{ __('Sign up') }}
                            </a>
                        </li>
                    @else
                        <li class="mr-3">
                            <form method="POST" action="{{ route('logout') }}">
                                @csrf
                                <a href="{{ route('logout') }}"
                                   onclick="event.preventDefault(); this.closest('form').submit();"
                                   class="no-underline hover:text-gray-500 py-2 px-4"
                                >
                                    {{ __('Log out') }}
                                </a>
                            </form>
                        </li>
                    @endguest
                </ul>
            </div>
        </header>
        <div class="flex content-center flex-wrap min-h-screen">
            <div class="w-1/2 mx-auto">
                <div class="text-center text-2xl font-bold text-white">
                    <p>
                        Bienvenue sur le site de la chaîne DevMaker !<br/><br/>
                        Nous sommes en train de développer quelques fonctionnalités sur notre site.<br/><br/>
                        D'ici là, viens jeter un œil sur notre chaine <a class="text-blue-800 underline" href="https://twitch.tv/devmaker_tv">Twitch</a> !<br/>
                    </p>
                </div>
            </div>
        </div>
    </body>
</html>
