<?php

namespace Tests\Unit;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class UserTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function user_has_expected_role()
    {
        $jr = User::factory()->create([
            'name' => 'Jr',
            'role' => 'admin',
        ]);

        $this->assertTrue($jr->isAdmin());

        $dobby = User::factory()->create([
            'name' => 'Dobby',
            'role' => 'user',
        ]);

        $this->assertFalse($dobby->isAdmin());
    }
}
